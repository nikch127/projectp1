# Dockerfile
FROM python:latest
WORKDIR /lab1
COPY factorial.py ./
COPY output/ ./output/
RUN pip install python-dotenv
CMD ["python", "./factorial.py"]
