import os
from dotenv import load_dotenv

def factorial(n):
    """
    Возвращает факториал числа n.
    """
    if n == 0 or n == 1:
        return 1
    else:
        return n * factorial(n - 1)

if __name__ == '__main__':
    load_dotenv()
    try:
        n = int(os.getenv('n'))
    except:
        n = 4
    
    result = factorial(n)
    print(f"Факториал числа {n} равен {result}")

    with open("output/result.txt", "w") as f:
        f.write(str(result))
